var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.cookie("access_token", "1234", {
    maxAge: 900000,
    path: "/",
    secure: true,
    httpOnly: true,
    sameSite: "strict",
  });
  res.cookie("refresh_token", "5678", {
    maxAge: 900000,
    path: "/refresh",
    secure: true,
    httpOnly: true,
    sameSite: "strict",
  });
  res.render("index", { message: "Set access_token & refresh_token" });
});

module.exports = router;
