var express = require("express");
var router = express.Router();

/* GET users listing. */
router.get("/", function (req, res, next) {
  // read the cookie
  var access_token = req.cookies.access_token;
  var refresh_token = req.cookies.refresh_token;
  console.log("access_token: ", access_token);
  console.log("refresh_token: ", refresh_token);
  // respond json
  res.json({
    access_token: access_token,
    refresh_token: refresh_token,
  });
});

module.exports = router;
